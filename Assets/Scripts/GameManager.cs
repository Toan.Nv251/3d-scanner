using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    [SerializeField] private string taskUuid;
    public string TaskUuid { get { return taskUuid; } set { taskUuid = value; } }
    void Awake()
    {
        TaskUuid = "35ea8740-5ef6-4e59-8577-3212735f90c9";
        GameObject[] objs = GameObject.FindGameObjectsWithTag("GameManager");
        if(Instance == null)
        {
            Instance = this;
        }

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }
}
