using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class ServerManager : Singleton<ServerManager>
{
    public delegate void CallBack();
    private TaskNewInit taskInit;
    public TaskNewInit TaskInit { get { return taskInit; } set { taskInit = value; } }
    //Init new Task
    public IEnumerator TaskNewInit(string url, CallBack callBack)
    {
        WWWForm form = new WWWForm();
        using (UnityWebRequest request = UnityWebRequest.Post(url, form))
        {
            yield return request.SendWebRequest();
            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    break;
                case UnityWebRequest.Result.DataProcessingError:
                    break;
                case UnityWebRequest.Result.Success:
                    taskInit = JsonUtility.FromJson<TaskNewInit>(request.downloadHandler.text);
                    GameManager.Instance.TaskUuid = taskInit.uuid;
                    callBack();
                    break;
            }
        }
    }
    //Upload image
    public IEnumerator TaskNewUpload(string url, TaskNewInit task, Texture2D texture, CallBack callBack)
    {
        string newURL = url + task.uuid;
        byte[] encodeToPNG;
        encodeToPNG = texture.EncodeToPNG();
        WWWForm form = new WWWForm();
        form.AddField("uuid", task.uuid);
        form.AddBinaryData("images", encodeToPNG);
        using (UnityWebRequest request = UnityWebRequest.Post(newURL, form))
        {
            yield return request.SendWebRequest();
            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    break;
                case UnityWebRequest.Result.DataProcessingError:
                    break;
                case UnityWebRequest.Result.Success:
                    callBack();
                    break;
            }
        }
    }
    //Commit Task
    public IEnumerator TaskNewCommit(string url, TaskNewInit task)
    {
        string newURL = url + task.uuid;
        WWWForm form = new WWWForm();
        form.AddField("uuid", task.uuid);
        using (UnityWebRequest request = UnityWebRequest.Post(newURL, form))
        {
            yield return request.SendWebRequest();
            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    break;
                case UnityWebRequest.Result.DataProcessingError:
                    break;
                case UnityWebRequest.Result.Success:
                    break;
            }
        }
    }
    //Get task info
    public IEnumerator TaskInfo(string url)
    {
        string newURl = url + taskInit.uuid + "/info";
        using (UnityWebRequest request = UnityWebRequest.Get(newURl))
        {
            yield return request.SendWebRequest();
            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    break;
                case UnityWebRequest.Result.DataProcessingError:
                    break;
                case UnityWebRequest.Result.Success:
                    taskInit = JsonUtility.FromJson<TaskNewInit>(request.downloadHandler.text);
                    break;
            }
        }
    }
    //Init task with setting
    public IEnumerator TaskNewInitWithSetting(string URL, string json, CallBack callBack)
    {
        using (UnityWebRequest request = UnityWebRequest.Post(URL, json))
        {
            byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);
            request.uploadHandler = new UploadHandlerRaw(jsonToSend);
            request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");

            yield return request.SendWebRequest();

            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    break;
                case UnityWebRequest.Result.DataProcessingError:
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log(request.downloadHandler.text);
                    taskInit = JsonUtility.FromJson<TaskNewInit>(request.downloadHandler.text);
                    GameManager.Instance.TaskUuid = taskInit.uuid;
                    callBack();
                    break;
            }

        }
    }
}
public class ServerURL
{
    public const string InitNewTaskURL = "http://10.30.176.136:3000/task/new/init";
    public const string TaskNewUpLoad = "http://10.30.176.136:3000/task/new/upload/";
    public const string TaskNewCommit = "http://10.30.176.136:3000/task/new/commit/";
    public const string TaskInfo = "http://10.30.176.136:3000/task/";
    public const string Info = "http://10.30.176.136:3000/task/info";
}
[System.Serializable]
public class TaskNewInit
{
    public string uuid;
    public string name;
    public int progress;
}