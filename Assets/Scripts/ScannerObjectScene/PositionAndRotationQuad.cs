using UnityEngine;

public class PositionAndRotationQuad : MonoBehaviour
{
    [SerializeField] private Material newMaterial;
    [SerializeField] private GameObject cube;
    private Vector3 positionQuad;
    public Vector3 PositionQuad { get { return positionQuad; } }
    private Vector3 rotationQuad;
    public Vector3 RotationQuad { get { return rotationQuad; } }
    private bool hitting = false;
    public bool Hitting { get { return hitting; } set { hitting = value; } }
    private bool hadTakenPhoto = false;
    public bool HadTakenPhoto { get { return hadTakenPhoto; } set { hadTakenPhoto = value; } }

    void Start()
    {
        positionQuad = gameObject.transform.position;
        rotationQuad = gameObject.transform.eulerAngles;
    }
    public void SetColorMaterialForCube(bool hadTakenPhoto)
    {
        if (hadTakenPhoto == true)
        {
            cube.gameObject.GetComponent<Renderer>().material = newMaterial;
        }
        else
        {
            return;
        }
    }
}
