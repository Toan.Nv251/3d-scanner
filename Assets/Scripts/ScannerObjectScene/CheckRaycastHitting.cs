using UnityEngine;

public class CheckRaycastHitting : MonoBehaviour
{
    public static CheckRaycastHitting Instance;
    [SerializeField] PositionAndRotationQuad[] arrayPositionAndRotationQuad;
    private Vector3 localParentRotation;
    private int count = 0;
    public int Count { get { return count; } set { count = value; } }
    public Vector3 LocalParentRotation
    {
        get
        {
            return localParentRotation;
        }
    }
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    void Start()
    {
        localParentRotation = new Vector3(gameObject.transform.eulerAngles.x, gameObject.transform.eulerAngles.y, gameObject.transform.eulerAngles.z);
    }
    public void ResetWhenRayCastNotHit()
    {
        for (int i = 0; i <= arrayPositionAndRotationQuad.Length; i++)
        {
            arrayPositionAndRotationQuad[i].Hitting = false;
            arrayPositionAndRotationQuad[i].GetComponent<Renderer>().material.color = new Color(0f, 238f, 255f, 40f);
        }
    }
    public void CheckCurrentQuadHitting()
    {
        foreach (var quad in arrayPositionAndRotationQuad)
        {
            if (quad.Hitting == true)
            {
                quad.transform.gameObject.GetComponent<PositionAndRotationQuad>().SetColorMaterialForCube(true);
                quad.transform.gameObject.GetComponent<PositionAndRotationQuad>().HadTakenPhoto = true;
            }
        }
    }
    public void HaveTakePhoto()
    {
        for (int i = 0; i <= arrayPositionAndRotationQuad.Length; i++)
        {
            arrayPositionAndRotationQuad[i].HadTakenPhoto = true;
            count += 1;
        }
    }
}
