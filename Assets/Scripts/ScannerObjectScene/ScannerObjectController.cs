using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class ScannerObjectController : MonoBehaviour
{
    public static ScannerObjectController Instance;
    [Header("Canvas Object")]
    [SerializeField] private GameObject initProcess;
    [SerializeField] private GameObject uploadImageProcess;
    [SerializeField] private GameObject loadingUI;
    [SerializeField] private TMP_Text textPercent;
    // [SerializeField] private GameObject loadToSceneShow;
    [Header("Button")]
    [SerializeField] private Button buttonInit;
    [SerializeField] private Button buttonInstance;
    [SerializeField] private Button buttonDelete;
    [SerializeField] private Button buttonTakeImage;
    [SerializeField] private Button buttonUploadImage;
    // [SerializeField] private Button buttonCommit;
    // [SerializeField] private Button buttonLoadScene;
    [Header("Parameter")]
    [SerializeField] private TextAsset jsonSetting;
    [SerializeField] private GameObject modelToInstance;
    [SerializeField] private GameObject positionScanner;
    [SerializeField] private GameObject modelScaler;
    private GameObject model;
    private PlacementIndicator placementIndicator;
    private bool checkStateProcess = false;
    private Camera cameraMain;
    private Vector3 rotationCameraMain;
    [SerializeField] private LayerMask mask;
    [SerializeField] private float cornerView;
    private float delayTime = 3f;
    private float time;
    [Header("Take photo")]
    [SerializeField] private GameObject[] UIs;
    private Texture2D screenTexture;
    //Scale object
    private float initialDistance;
    private Vector3 initialScale;
    private bool isCommited;
    [SerializeField] private float minScale, maxScale;
    
    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }
    void Start()
    {
        time = 0f;
        isCommited = false;
        buttonTakeImage.interactable = false;
        buttonUploadImage.interactable = false;
        buttonInit.onClick.AddListener(InitNewTaskWithSetting);
        buttonInstance.onClick.AddListener(InstanceModel);
        buttonDelete.onClick.AddListener(DeleteModel);
        buttonTakeImage.onClick.AddListener(TakePhoto);
        // buttonLoadScene.onClick.AddListener(LoadScene);
        buttonUploadImage.onClick.AddListener(CommitTask);
        placementIndicator = FindObjectOfType<PlacementIndicator>();
        cameraMain = Camera.main;
        screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
    }
    void Update()
    {
        if (isCommited == true)
        {
            time += Time.deltaTime;
            while (time >= delayTime)
            {
                StartCoroutine(ServerManager.Instance.TaskInfo(ServerURL.TaskInfo));
                if (ServerManager.Instance.TaskInit.progress < 100)
                {
                    textPercent.text = $"Loading: {ServerManager.Instance.TaskInit.progress} %";
                }
                else if (ServerManager.Instance.TaskInit.progress == 100)
                {
                    isCommited = false;
                    SceneManager.LoadScene(1);
                }
                time -= delayTime;
            }
        }
        if (model != null)
        {
            foreach (Touch touch in Input.touches)
            {
                if (Input.touchCount == 2)
                {
                    var touchZero = Input.GetTouch(0);
                    var touchOne = Input.GetTouch(1);
                    if (touchZero.phase == TouchPhase.Ended || touchZero.phase == TouchPhase.Canceled ||
                        touchOne.phase == TouchPhase.Ended || touchOne.phase == TouchPhase.Canceled)
                    {
                        return;
                    }

                    if (touchZero.phase == TouchPhase.Began || touchOne.phase == TouchPhase.Began)
                    {
                        initialDistance = Vector2.Distance(touchZero.position, touchOne.position);
                        initialScale = model.transform.localScale;
                    }
                    else
                    {
                        var currentDistance = Vector2.Distance(touchZero.position, touchOne.position);

                        if (Mathf.Approximately(initialDistance, 0))
                        {
                            return;
                        }

                        var factor = currentDistance / initialDistance;
                        model.transform.localScale = initialScale * factor;
                    }
                }
            }
            Vector3 rayOrigin = new Vector3(0.5f, 0.5f, 0f);
            Ray ray = cameraMain.ViewportPointToRay(rayOrigin);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1, mask))
            {
                switch (hit.transform.GetComponent<PositionAndRotationQuad>().HadTakenPhoto)
                {
                    case false:
                        rotationCameraMain = cameraMain.transform.eulerAngles;
                        if (rotationCameraMain.x >= hit.transform.GetComponent<PositionAndRotationQuad>().RotationQuad.x - cornerView
                        && rotationCameraMain.x <= hit.transform.GetComponent<PositionAndRotationQuad>().RotationQuad.x + cornerView
                        && rotationCameraMain.y >= hit.transform.GetComponent<PositionAndRotationQuad>().RotationQuad.y - cornerView
                        && rotationCameraMain.y <= hit.transform.GetComponent<PositionAndRotationQuad>().RotationQuad.y + cornerView)
                        {
                            hit.transform.GetComponent<Renderer>().material.color = new Color(255f, 0f, 0f, 0.2f);
                            hit.transform.GetComponent<PositionAndRotationQuad>().Hitting = true;
                            buttonTakeImage.interactable = true;
                        }
                        break;
                    case true:
                        buttonTakeImage.interactable = false;
                        break;
                }
            }
            else
            {
                buttonTakeImage.interactable = false;
                CheckRaycastHitting.Instance.ResetWhenRayCastNotHit();
                CheckRaycastHitting.Instance.HaveTakePhoto();
                // if(CheckRaycastHitting.Instance.Count >= 5)
                // {
                //     buttonUploadImage.interactable = true;
                // }
            }
        }
    }
    private void CheckStateUI()
    {
        switch (checkStateProcess)
        {
            case false:
                initProcess.gameObject.SetActive(true);
                uploadImageProcess.gameObject.SetActive(false);
                break;
            case true:
                initProcess.gameObject.SetActive(false);
                uploadImageProcess.gameObject.SetActive(true);
                break;
        }
    }
    private void InitNewTask()
    {
        StartCoroutine(ServerManager.Instance.TaskNewInit(ServerURL.InitNewTaskURL, CheckStateUI));
        checkStateProcess = true;
    }
    private void InitNewTaskWithSetting()
    {
        // Debug.Log(jsonSetting.text);
        StartCoroutine(ServerManager.Instance.TaskNewInitWithSetting(ServerURL.InitNewTaskURL, jsonSetting.text, CheckStateUI));
        checkStateProcess = true;
    }
    private void InstanceModel()
    {
        if (model == null)
        {
            model = Instantiate(modelToInstance, placementIndicator.transform.position, placementIndicator.transform.rotation);
            placementIndicator.gameObject.SetActive(false);
            positionScanner.gameObject.SetActive(true);
        }
    }
    private void DeleteModel()
    {
        if (model != null)
        {
            Destroy(model.gameObject);
            placementIndicator.gameObject.SetActive(true);
            positionScanner.gameObject.SetActive(false);
        }
    }
    private void TakePhoto()
    {
        buttonDelete.interactable = false;
        CheckRaycastHitting.Instance.CheckCurrentQuadHitting();
        foreach (var uiObject in UIs)
        {
            uiObject.gameObject.SetActive(false);
        }
        if (model != null)
        {
            model.gameObject.SetActive(false);
        }
        StartCoroutine(screenShot());
    }
    private IEnumerator screenShot()
    {
        buttonUploadImage.interactable = true;
        yield return new WaitForEndOfFrame();
        Rect regionToRead = new Rect(0f, 0f, Screen.width, Screen.height);
        screenTexture.ReadPixels(regionToRead, 0, 0, false);
        screenTexture.Apply();
        loadingUI.gameObject.SetActive(true);
        StartCoroutine(ServerManager.Instance.TaskNewUpload(ServerURL.TaskNewUpLoad, ServerManager.Instance.TaskInit, screenTexture, Loading));
        foreach (var uiObject in UIs)
        {
            uiObject.gameObject.SetActive(true);
        }
        if (model != null)
        {
            model.gameObject.SetActive(true);
        }
    }
    private void Loading()
    {
        loadingUI.gameObject.SetActive(false);
    }
    private void CommitTask()
    {
        isCommited = true;
        loadingUI.gameObject.SetActive(true);
        StartCoroutine(ServerManager.Instance.TaskNewCommit(ServerURL.TaskNewCommit, ServerManager.Instance.TaskInit));

    }
    // private void LoadScene()
    // {
    //     SceneManager.LoadScene(1);
    // }
}
