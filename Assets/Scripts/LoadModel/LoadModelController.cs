using UnityEngine;
using TriLibCore;
using TMPro;
using UnityEngine.UI;

public class LoadModelController : MonoBehaviour
{
    private GameObject objectClone;
    [SerializeField] private GameObject loadingUI;
    [SerializeField] private TMP_Text loadingText;
    [SerializeField] private Button buttonExit;
    private string ModelURL;
    void Awake()
    {
        // ServerManager.Instance.Task.uuid = "35ea8740-5ef6-4e59-8577-3212735f90c9";
#if UNITY_ANDROID
        ModelURL = "http://10.30.176.136:3000/task/" + GameManager.Instance.TaskUuid + "/download/texture_model.zip";
#endif
#if UNITY_EDITOR
        ModelURL = "http://10.30.176.136:3000/task/7c144bc3-5de8-47d7-953f-d33dd7517416/download/texture_model.zip";
#endif
    }
    private void Start()
    {
        buttonExit.onClick.AddListener(QuitApp);
        loadingUI.gameObject.SetActive(true);
        var assetLoaderOptions = AssetLoader.CreateDefaultLoaderOptions();
        var webRequest = AssetDownloader.CreateWebRequest(ModelURL);
        AssetDownloader.LoadModelFromUri(webRequest, OnLoad, OnMaterialsLoad, OnProgress, OnError, null, assetLoaderOptions);
    }
    private void OnLoad(AssetLoaderContext assetLoaderContext)
    {

    }
    private void OnMaterialsLoad(AssetLoaderContext assetLoaderContext)
    {

    }
    private void OnProgress(AssetLoaderContext assetLoaderContext, float progress)
    {
        loadingText.text = $"Loading: {Mathf.Round(progress*100)} %";
        if (progress >= 1f)
        {
            objectClone = GameObject.Find("1");
            objectClone.gameObject.tag = "Model";
            objectClone.AddComponent<ModelController>();
            objectClone.AddComponent<BoxCollider>();
            loadingUI.gameObject.SetActive(false);
        }
    }
    private void OnError(IContextualizedError obj)
    {
        Debug.Log($"An error occurred while loading your Model: {obj.GetInnerException()}");
        loadingText.text = "Can't connect to Server, please try again !!!";
        buttonExit.gameObject.SetActive(true);
    }
    private void QuitApp()
    {
        Application.Quit();
    }
}
